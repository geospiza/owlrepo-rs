use fltk::{app::*, button::*, dialog::*, draw, frame::*, image::*, table::*, window::*};
use std::cell::RefCell;
use std::fs;
use std::path::PathBuf;
use std::rc::Rc;

fn new_table() -> Table {
    let mut table = Table::new(40, 25 + 20 + 40, 400, 600, "");
    table.set_row_resize(true);
    table.set_row_header(true);
    table.set_cols(1);
    table.set_col_resize(true);
    table.set_col_width_all(340);
    table.end();
    return table;
}

fn main() {
    let app = App::default();
    let mut wind = Window::new(100, 100, 1024, 768, "owlrepo-rs");
    let mut source_but = Button::new(40, 25, 80, 40, "Set Source");
    let mut run_but = Button::new(120 + 20, 25, 80, 40, "Run");
    let frame = Rc::from(RefCell::new(Frame::new(500, 150, 435, 380, "")));
    let table = Rc::from(RefCell::new(new_table()));
    let table_data: Rc<RefCell<Vec<PathBuf>>> = Rc::from(RefCell::new(vec![]));
    let table_cell = Rc::from(RefCell::new(PathBuf::new()));

    wind.end();
    wind.show();

    let table_c = table.clone();
    let table_data_c = table_data.clone();
    source_but.set_callback(Box::new(move || {
        let directory = dir_chooser("Choose Directory", "C://MapleLegendsHD", true).unwrap();
        set_data(&table_c, &table_data_c, &directory);
        println!("{}", directory);
    }));

    //the source button is useless, let's assume we have a default already
    run_but.set_callback(Box::new(move || {
        println!("run button does nothing yet");
    }));

    set_data(&table, &table_data, "C://MapleLegendsHD/Screenshots");

    let table_c = table.clone();
    let table_data_c = table_data.clone();
    let table_cell_c = table_cell.clone();
    &table
        .borrow_mut()
        .draw_cell(Box::new(move |ctx, row, col, x, y, w, h| match ctx {
            TableContext::RowHeader => draw_header(&format!("{}", row + 1), x, y, w, h), // Row titles
            TableContext::Cell => {
                if (row as usize) >= *&table_data_c.borrow().len() {
                    return;
                }
                let path = &table_data_c.borrow()[row as usize];
                if table_c.borrow().is_selected(row, col) {
                    table_cell_c.replace(path.clone());
                }
                draw_data(
                    path.file_name().unwrap().to_str().unwrap(),
                    x,
                    y,
                    w,
                    h,
                    table_c.borrow().is_selected(row, col),
                );
            }
            _ => (),
        }));

    let table_cell_c = table_cell.clone();
    let frame_c = frame.clone();
    &table.borrow_mut().handle(Box::new(move |ev| match ev {
        Event::Push => {
            if event_clicks() {
                // double clicks
                let c = table_cell_c.borrow();
                println!("{:?}", c);
                let image = SharedImage::load(&c).unwrap();
                &frame_c.borrow_mut().set_image(Some(image));
                &frame_c.borrow_mut().redraw();
                return true;
            }
            false
        }
        _ => false,
    }));
    app.run().unwrap();
}

fn set_data(table: &Rc<RefCell<Table>>, table_data: &Rc<RefCell<Vec<PathBuf>>>, directory: &str) {
    println!("{}", directory);
    let paths = fs::read_dir(directory).unwrap();
    let data: Vec<PathBuf> = paths
        .into_iter()
        .map(|p| fs::canonicalize(p.unwrap().path()).unwrap())
        .collect();
    println!("{} items ex: {:?}", data.len(), data.first().unwrap());
    table.borrow_mut().set_rows(data.len() as u32);
    table_data.replace(data);
}

fn draw_header(s: &str, x: i32, y: i32, w: i32, h: i32) {
    draw::push_clip(x, y, w, h);
    draw::draw_box(FrameType::ThinUpBox, x, y, w, h, Color::FrameDefault);
    draw::set_draw_color(Color::Black);
    draw::draw_text2(s, x, y, w, h, Align::Center);
    draw::pop_clip();
}
// https://github.com/MoAlyousef/fltk-rs/blob/master/examples/spreadsheet.rs
// The selected flag sets the color of the cell to a grayish color, otherwise white
fn draw_data(s: &str, x: i32, y: i32, w: i32, h: i32, selected: bool) {
    draw::push_clip(x, y, w, h);
    if selected {
        draw::set_draw_color(Color::from_u32(0xD3D3D3));
    } else {
        draw::set_draw_color(Color::White);
    }
    draw::draw_rectf(x, y, w, h);
    draw::set_draw_color(Color::Gray0);
    draw::draw_text2(s, x, y, w, h, Align::Center);
    draw::draw_rect(x, y, w, h);
    draw::pop_clip();
}
